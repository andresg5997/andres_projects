import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  nameTitle: {
    fontSize: 20,
    textAlign: 'center',
  },
  nameValue: {
    fontWeight: 'bold',
    fontSize: 32,
  },
  quantityNumber: {
    fontWeight: 'bold',
    fontSize: 32,
  },
  quantityTitle: {
    fontSize: 20,
    textAlign: 'center',
  },
  priceNumber: {
    fontWeight: 'bold',
    fontSize: 28,
  },
  priceTitle: {
    fontSize: 20,
    textAlign: 'center',
  },
  componentsTitle: {
    fontSize: 20,
  },
  componentItem: {
    fontWeight: 'bold',
    fontSize: 22,
  },
  componentIndicator: {
    fontWeight: 'normal',
    fontSize: 22,
  },
  failedMessageTitle: {
    fontWeight: 'bold',
    fontSize: 28,
    textAlign: 'center',
  },
  failedMessageSubtitle: {
    fontWeight: 'bold',
    fontSize: 22,
    textAlign: 'center',
  },
});

export default styles;
