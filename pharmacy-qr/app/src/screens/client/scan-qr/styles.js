import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
    padding: 16,
  },
});

export default styles;
