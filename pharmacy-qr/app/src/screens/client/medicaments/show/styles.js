import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  noResultsTitle: {
    fontWeight: 'bold',
    fontSize: 28,
    textAlign: 'center',
  },
});

export default styles;
