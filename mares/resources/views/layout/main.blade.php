@include('layout.header')

<div class="grey lighten-2">
	@yield('content')
</div>

@include('layout.footer')