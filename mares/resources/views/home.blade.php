@extends('layout.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col m8 offset-m2">
            <div class="card">
                <div class="card-content center">
                    <div class="card-title">¡Ya iniciaste sesión!</div>
                    <a class="btn waves-effect waves-light white-text" href="/">Ir a Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
