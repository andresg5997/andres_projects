import variable from '../variables';

export default (variables = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    color: '#9E9E9E',
  };

  return iconTheme;
};
