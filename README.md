# Andres Projects

This is a repository made to show the ongoing and already made projects that I have worked on.

## Universia Server

A RESTful API for a sports portal.

**Languages used:** JavaScript.

**Frameworks/other technologies:** NodeJS, Express, Mongoose.

**Made by:** Only me.

## GPS Compliance

A simple information website.

**Languages used:** HTML/CSS, JavaScript.

**Frameworks/other technologies:** SASS.

**Made by:** Only me.

## Pharmacy QR

A mobile app for medicaments purchase through the use of QR code/scanner.

**Languages used:** JavaScript.

**Frameworks/other technologies:** ReactNative.

**Made by:** Only me.

## Financial Planner

A mobile app for a personal finances.

**Languages used:** JavaScript.

**Frameworks/other technologies:** ReactNative.

**Made by:** Me and a colleague.

## Mares

A work collaboration intranet for a law firm.

**Languages used:** PHP, JavaScript.

**Frameworks/other technologies:** Laravel, Vue.JS.

**Made by:** Me and a colleague.


